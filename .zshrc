# Created by newuser for 5.0.2

# 環境変数
export LANG=ja_JP.UTF-8
#export LANG=C


# 色を使用出来るようにする
autoload -Uz colors
colors

# ヒストリの設定
HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=1000000

# プロンプト
# 1行表示
# PROMPT="%~ %# "
# 2行表示
PROMPT="%{${fg[green]}%}[%n@%m]%{${reset_color}%} %~
%# "

# 同じコマンドをヒストリに残さない
setopt hist_ignore_all_dups

# pyenv
export PYENV_ROOT="$HOME/.pyenv"
eval "$(pyenv init -)"
##

source ~/.alias
export PATH="$PATH:/home/ryshibat/bin:/usr/local/opt/gettext/bin:$PYENV_ROOT/bin"
source ~/.cargo/env

